# 2021 05 05 Code Traceability

Support and documentation from the 20210505 OCAPY virtual meetup.

- presentation by Nicolas Bruot on how to improve the traceability of ones' python code
- short introduction by Frantz Martinache on how to set up / use pyenv
